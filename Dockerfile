FROM postgres:13.0

RUN apt update \
    && apt -y install --no-install-recommends gnupg2 awscli \
    && apt -y clean \
    && useradd -ms /bin/bash pg_s3_backup

COPY pg_s3_backup /usr/bin
USER pg_s3_backup
WORKDIR /home/pg_s3_backup
ENTRYPOINT ["/usr/bin/pg_s3_backup"]
